// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token
// 同时，我们也可以在此使用getApp().globalData，如果你把token放在getApp().globalData的话，也是可以使用的
const install = (Vue, vm) => {
	Vue.prototype.$u.http.setConfig({
		// baseUrl: 'http://yewh.free.idcfengye.com/api',
		baseUrl: 'https://ywh.nqvqn.com/api',
		// baseUrl: 'http://localhost:8352/api',
		// 如果将此值设置为true，拦截回调中将会返回服务端返回的所有数据response，而不是response.data
		// 设置为true后，就需要在this.$u.http.interceptor.response进行多一次的判断，请打印查看具体值
		originalData: true,
		// 设置自定义头部content-type
		loadingText: '加载中~',
		loadingTime: 1800,
		header: {
			'content-type': 'application/x-www-form-urlencoded'
		}
	});
	// 请求拦截，配置Token等参数
	Vue.prototype.$u.http.interceptor.request = (config) => {
		// config.header.Token = 'xxxxxx';

		// 方式一，存放在vuex的token，假设使用了uView封装的vuex方式，见：https://uviewui.com/components/globalVariable.html
		// config.header.token = vm.token;

		// 方式二，如果没有使用uView封装的vuex方法，那么需要使用$store.state获取
		// config.header.token = vm.$store.state.token;

		// 方式三，如果token放在了globalData，通过getApp().globalData获取
		// config.header.token = getApp().globalData.username;

		// 方式四，如果token放在了Storage本地存储中，拦截是每次请求都执行的，所以哪怕您重新登录修改了Storage，下一次的请求将会是最新值
		const token = uni.getStorageSync('token');
		if(token && typeof token != 'undefined'){
			config.header.token = token;
		}
		return config;
	}
	// 响应拦截，判断状态码是否通过
	Vue.prototype.$u.http.interceptor.response = (response) => {
		let res = response.data;
		if (res.code === 1) {
			return res.data;
		} else if (res.code === 0)  {
			vm.$u.toast(res.msg);
			return false;
		} else if ( res.code === 401){
			const currentPages = getCurrentPages()[getCurrentPages().length-1].$page.fullPath;
			// 如果是个人中心，不用提示和调转
			if(currentPages!=='/pages/uCenter/index'){
				// 提示请登录后操作
				vm.$u.toast(res.msg);
				// 跳到个人中心
				vm.$u.route({
					type: 'tab',
					url: 'pages/uCenter/index'
				})
			}

			return false;
		}else {
			vm.$u.toast(res.msg)
			return false;
		}
	}
}

export default {
	install
}
