// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
// 微博列表
let weiboListUrl = '/weibo/list'
// 微信授权接口
let userThirdUrl = '/user/weixinMinProgramLogin'
// 微信小程序注册登录接口
let weixinRegisterUrl = '/user/weixinRegister'


// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	// 微博列表
	let getWeiboList = (params = {}) => vm.$u.get(weiboListUrl, params);
	// 微信授权
	let userThird = (params = {}) => vm.$u.post(userThirdUrl, params);
	// 微信注册或登录
	let weixinRegister = (params = {}) => vm.$u.post(weixinRegisterUrl, params);
	// 点击大图（详情）
	let userLookDetail = (params = {}) => vm.$u.post('/weibo/userLookDetail', params);
	// 点赞
	let userLike = (params = {}) => vm.$u.post('/weibo/userLike', params);
	// 用户昵称头像
	let getUserInfo = (params = {}) => vm.$u.post('/user/index', params);
	// 下载图片
	let userDownload = (params = {}) => vm.$u.post('/weibo/userDownload', params);
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {getWeiboList,userThird,weixinRegister,userLookDetail,userLike
		,getUserInfo,userDownload};
}

export default {
	install
}
